﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.DependencyInjection;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using Object = UnityEngine.Object;

public class DependencyManager : SerializedMonoBehaviour {
    public List<MonoBehaviour> dependencies;
    private List<DependencyType> dependencyInterfaces = new List<DependencyType>();

    public static DependencyManager instance;
    
    private static List<MonoBehaviour> cachedComponents = new List<MonoBehaviour>();


    private void Awake() {
        if (instance == null) {
            instance = this;
        }

        foreach (var dependency in dependencies) {
            var type = dependency.GetType();
            var interfaces = type.GetInterfaces();
            foreach (var interf in interfaces) {
                dependencyInterfaces.Add(new DependencyType {dependency = dependency, type = interf});
            }
        }


        Object[] components = GameObject.FindObjectsOfType(typeof(MonoBehaviour));

        foreach (var component in components) {
            ResolveDependencies(component);
        }
    }

    public void ResolveDependencies(Object component) {
        var type = component.GetType();
        var propertyInfos = type.GetProperties();

        foreach (var propertyInfo in propertyInfos) {
            if (!propertyInfo.PropertyType.IsInterface || propertyInfo.GetAttribute<ResolveDependency>() == null) {
                continue;
            }
            
            if (propertyInfo.GetValue(component) == null) {
                var dependencyTypes = dependencyInterfaces.Where(dependencyType =>
                    dependencyType.type == propertyInfo.PropertyType);
                if (dependencyTypes.Any()) {
                    propertyInfo.SetValue(component, dependencyTypes.First().dependency);
                }
            }
        }
    }


    public static GameObject RegisteredInstantiate(GameObject gameObject) {
        return RegisteredInstantiate(gameObject, Vector3.zero, Quaternion.identity);
    }

    public static GameObject RegisteredInstantiate(GameObject gameObject, Vector3 position) {
        return RegisteredInstantiate(gameObject, position, Quaternion.identity);
    }

    public static GameObject RegisteredInstantiate(GameObject gameObject, Vector3 position, Quaternion rotation,
        Transform parent = null)
    {
        var instantiatedGameObject = Instantiate(gameObject, position, rotation, parent);

        // Temp. fix to make tests run without dependency manager
        // TODO: Implement better solution
        if (instance == null) return instantiatedGameObject;
        
        instantiatedGameObject.GetComponentsInChildren(true, cachedComponents);

        foreach (var component in cachedComponents) {
            instance.ResolveDependencies(component);
        }

        cachedComponents.Clear();
        return instantiatedGameObject;
    }

    struct DependencyType {
        public MonoBehaviour dependency;
        public Type type;
    }
}