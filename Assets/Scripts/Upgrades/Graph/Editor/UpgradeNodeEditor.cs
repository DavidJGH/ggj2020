using UnityEngine;
using Upgrades.Graph;
using XNode;
using XNodeEditor;

namespace DefaultNamespace.Graph.Editor {
    [CustomNodeEditor(typeof(UpgradeNode))]
    public class UpgradeNodeEditor : NodeEditor {

        public override Color GetTint() {
            Color color = Color.gray;

            if (IsRootNode()) {
                color = new Color(0.82f, 0.41f, 0.17f);
            }
            else if (IsUnlocked()) {
                color = new Color(0.21f, 0.22f, 0.78f);
            }

            return color;
        }

        private bool IsUnlocked() {
            UpgradeNode upgradeNode = (UpgradeNode) target;
            return upgradeNode.isUnlocked;
        }

        private bool IsRootNode() {
            UpgradeNode upgradeNode = (UpgradeNode) target;
            return upgradeNode.UpgradeGraph.rootNode == upgradeNode;
        }

        public override void OnBodyGUI() {
            NodePort input = target.GetPort(nameof(UpgradeNode.input));
            NodePort output = target.GetPort(nameof(UpgradeNode.output));

            if (!IsRootNode() && output != null) {
                NodeEditorGUILayout.PortField(new Vector2(GetWidth() / 2f, 0), output);
            }

            base.OnBodyGUI();

            if (input != null) {
                NodeEditorGUILayout.PortField(
                    new Vector2(GetWidth() / 2f, GUILayoutUtility.GetLastRect().position.y + 20f),
                    input);
            }
        }
    }
}