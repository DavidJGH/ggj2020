using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using UnityEngine;

namespace Salvaging
{
    public class SpawnerManager : SerializedMonoBehaviour
    {
        [OdinSerialize] private float _baseSpawnDensity = 0.0001f;
        [OdinSerialize] private float _respawnInterval = 60;

        [OdinSerialize] public List<ScrapSpawner> Spawners { get; set; }
        [OdinSerialize] private List<GameObject> _defaultSpawnedPrefabs;

        private void Start()
        {
            foreach (var spawner in Spawners)
            {
                if (_defaultSpawnedPrefabs != null && spawner.ScrapPrefabs.IsNullOrEmpty())
                {
                    spawner.ScrapPrefabs = _defaultSpawnedPrefabs;
                }
                else
                {
                    Debug.LogWarning($"Spawner {spawner} has no scrap prefabs and no global prefabs are set.");
                }

                spawner.Spawn(GetSpawnDensity(spawner));
            }

            StartCoroutine(CRespawn());
        }

        private float GetSpawnDensity(ScrapSpawner spawner)
        {
            var upperLayerBound = GetLayer(spawner.SpawnArea).StartsAtY;
            var lowestLowerLayerBound = LayersManager.Instance.lastY;
            return _baseSpawnDensity * (upperLayerBound / lowestLowerLayerBound + .2f);
        }

        private IEnumerator CRespawn()
        {
            while (true)
            {
                Debug.Log("respawn");
                foreach (var spawner in Spawners.Where(spawner => GetLayer(spawner.SpawnArea) != LayersManager.Instance.CurrentLayer))
                {
                    Debug.Log(spawner);
                    spawner.Spawn(GetSpawnDensity(spawner));
                }

                yield return new WaitForSeconds(_respawnInterval);
            }
        }
        
        private Layer GetLayer(BoxCollider boxCollider)
        {
            return LayersManager.Instance.Layers
                .OrderBy(layer => layer.StartsAtY)
                .FirstOrDefault(layer => boxCollider.bounds.max.y < layer.StartsAtY);
        }
    }
}