﻿Shader "Custom/UnderwaterShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseTex ("Noise", 2D) = "white" {}
        _FogColour ("Fog Colour", Color) = (1, 1, 1, 1)
        _FogDensity ("Fog Density", float) = 1
        _FogOpacity ("Fog Opacity", float) = 1
        _Scale ("Scale", float) = 1
        _Speed ("Speed", float) = 1
        _Impact ("Impact", float) = 1
        _ColorGrading ("Color Grading", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            Stencil
            {
                Ref 1
                Comp NotEqual
            }
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.screenPos = ComputeScreenPos(o.vertex);
                o.uv = v.uv;
                
                return o;
            }

            sampler2D _MainTex;
            uniform sampler2D _CameraDepthTexture;
            sampler2D _NoiseTex;
            
            float _Scale;
            float _Speed;
            float _Impact;
            float _FogDensity;
            float _FogOpacity;
            
            fixed4 _FogColour;
            fixed4 _ColorGrading;
            
            float2 snoise(float3 pos){
                return tex2D(_NoiseTex, pos.xy + sin(pos.z*5));
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                float3 pos = float3(i.screenPos.x, i.screenPos.y, 0) * _Scale;
                pos.z = _Time[0] * _Speed;
                float noise = (snoise(pos).x+1) / 2;
                float4 directional = float4(cos(6.28318530718 * noise), sin(6.28318530718 * noise), 0, 0);
                fixed4 col = tex2Dproj(_MainTex, i.screenPos + normalize(directional) * _Impact);
                float depth = UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,  i.screenPos + normalize(directional) * _Impact));
                
                col *= _ColorGrading;
                
                return lerp(col, _FogColour * _FogOpacity + col * (1-_FogOpacity), exp ( -depth * _FogDensity ));
            }
            ENDCG
        }
    }
}
