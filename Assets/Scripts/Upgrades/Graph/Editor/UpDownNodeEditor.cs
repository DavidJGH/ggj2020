﻿using UnityEngine;
using XNode;
using XNodeEditor;

namespace Upgrades.Graph.Editor {
    [CustomNodeEditor(typeof(UpDownNode))]
    public class UpDownNodeEditor : NodeEditor {
        protected bool hideOutputNode = false;
        
        public override void OnBodyGUI() {
            NodePort input = target.GetPort(nameof(UpgradeNode.input));
            NodePort output = target.GetPort(nameof(UpgradeNode.output));

            if (!hideOutputNode && output != null) {
                NodeEditorGUILayout.PortField(new Vector2(GetWidth() / 2f, 0), output);
            }

            base.OnBodyGUI();

            if (input != null) {
                NodeEditorGUILayout.PortField(
                    new Vector2(GetWidth() / 2f, GUILayoutUtility.GetLastRect().position.y + 20f),
                    input);
            }
        }
    }
}