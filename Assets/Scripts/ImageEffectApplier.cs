﻿using UnityEngine;

public class ImageEffectApplier : MonoBehaviour
{
    public Material material;

    private void Update()
    {
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }
}