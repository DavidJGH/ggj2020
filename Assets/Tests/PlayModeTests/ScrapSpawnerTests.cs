using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Salvaging;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ScrapSpawnerTests
    {
        private ScrapSpawner _spawner;
        private BoxCollider _spawnArea;

        private const string ScrapName = "Scrap";

        [SetUp]
        public void SetUp()
        {
            var spawnerPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/ScrapSpawner.prefab");
            var scrapPrefab = new GameObject(ScrapName);

            var spawnerInstance = Object.Instantiate(spawnerPrefab);
            _spawner = spawnerInstance.GetComponent<ScrapSpawner>();
            _spawner.ScrapPrefabs = new List<GameObject> {scrapPrefab};

            _spawnArea = spawnerInstance.GetComponent<BoxCollider>();
        }

        [UnityTest]
        public IEnumerator SpawnsScrapInBoundsWithDensity()
        {
            var spawnAreaSize = _spawnArea.size;
            var volume = spawnAreaSize.x * spawnAreaSize.y * spawnAreaSize.z;
            var density = .0005f;

            _spawner.Spawn(density);

            var spawnedScraps = FindAllScrap()
                .Count(gameObject => _spawnArea.bounds.Contains(gameObject.transform.position));

            Assert.AreEqual(volume * density, spawnedScraps, 1);
            yield break;
        }

        [UnityTest]
        public IEnumerator DoesNotExceedMaxAmountOfScrap()
        {
            var density = .0005f;

            _spawner.Spawn(density);

            var spawnedScraps = FindAllScrap().ToList();
            var numOfSpawnedScraps = spawnedScraps.Count;
            foreach (var someScrap in spawnedScraps.GetRange(0, 3))
            {
                Object.Destroy(someScrap);
            }

            _spawner.Spawn(density);

            var newNumOfSpawnedScraps = FindAllScrap().Count();

            Assert.AreEqual(newNumOfSpawnedScraps, numOfSpawnedScraps);
            yield break;
        }

        private IEnumerable<GameObject> FindAllScrap()
        {
            return Object.FindObjectsOfType(typeof(GameObject))
                .Select(gameObject => (GameObject) gameObject)
                .Where(gameObject => gameObject.name.Contains(ScrapName));
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var gameObject in Object.FindObjectsOfType(typeof(GameObject)))
            {
                Object.Destroy(gameObject);
            }
        }
    }
}