﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using UnityEngine;
using Upgrades.Graph.Logic;
using XNode;

namespace Upgrades.Graph {
    [Serializable]
    public abstract class UpgradeNode : Node {
        [OdinSerialize]
        public int cost;
        [OdinSerialize]
        public bool isUnlocked;

        /// <summary>
        /// Decides whether all parents have to be unlocked or just one, look at <see cref="MultipleParentOperator"/> for more info
        /// </summary>
        [HideInInspector]
        public bool unlockModeIsAny;

        public bool IsBlocked { get; private set; }

        public Transform Transform { get; private set; }

        public ISet<UpgradeNode> Parents { get; set; }

        //children
        [Input, HideInInspector]
        public UpgradeNode input;

        //the node itself
        [Output(connectionType: ConnectionType.Override), HideInInspector]
        public UpgradeNode output;

        public UpgradeGraph UpgradeGraph => graph as UpgradeGraph;

        private UpgradeNode[] Children => GetInputPort(nameof(input)).GetInputValues<UpgradeNode>();

        /// <summary>
        /// Consult david for this monstrosity
        /// </summary>
        public bool IsUnlockable {
            get {
                return (
                           unlockModeIsAny
                               ? Parents.Any(parent => parent.isUnlocked)
                               : Parents.All(parent => parent.IsBlocked || parent.isUnlocked)
                                 || Parents.Count == 0) &&
                       !IsBlocked;
            }
        }

        #region xNode Specific Methods

        [ContextMenu("Make Root Node")]
        public void MakeRootNode() {
            UpgradeGraph.rootNode = this;
            GetInputPort(nameof(input)).ClearConnections();
        }

        public override object GetValue(NodePort port) {
            if (port.fieldName == nameof(output)) {
                return this;
            }

            return base.GetValue(port);
        }

        [Button]
        public void LogChildrenTypes() {
            foreach (var child in GetAllChildren()) {
                Debug.Log(child.GetType().ToString());
            }
        }

        [Button]
        public void LogParentTypes() {
            foreach (var parent in Parents) {
                Debug.Log(parent.GetType().ToString());
            }
        }

        #endregion

        #region UpgradeLogic

        public void Initialise(Transform transform) {
            Transform = transform;
            foreach (var child in Children) {
                bool uninitialised = child.Parents is null;
                if (uninitialised)
                    child.Parents = new HashSet<UpgradeNode>();

                child.Parents.Add(this);

                if (uninitialised)
                    child.Initialise(transform);
            }

            if (Parents is null) {
                Parents = new HashSet<UpgradeNode>();
                InitialiseUnlocked();
            }
        }

        public void InitialiseUnlocked() {
            if (isUnlocked)
                Unlock();

            foreach (var child in GetAllChildren()) {
                if (child.isUnlocked)
                    child.Unlock();
            }
        }

        public bool Unlock() {
            if (IsUnlockable) {
                isUnlocked = true;
                OnUnlock();
                OnEnableUpgrade();
                return true;
            }

            isUnlocked = false;
            return false;
        }

        public void MakeNewRoot() {
            var children = GetAllChildren();
            var root = GetRoot();

            foreach (var node in root.GetAllChildren()) {
                if (!children.Contains(node)) {
                    node.IsBlocked = true;
                    node.OnDisableUpgrade();
                }
            }
        }

        public List<UpgradeNode> GetAllChildren() {
            return GetAllChildrenRecursive().Distinct().ToList();
        }

        private List<UpgradeNode> GetAllChildrenRecursive() {
            var children = new List<UpgradeNode>();
            foreach (var child in Children) {
                children.Add(child);
                children.AddRange(child.GetAllChildren());
            }

            return children;
        }

        public ISet<UpgradeNode> GetAllParents() {
            return GetAllParentsRecursive().Distinct().ToHashSet();
        }

        public ISet<UpgradeNode> GetAllParentsRecursive() {
            var parents = new HashSet<UpgradeNode>();
            foreach (var parent in Parents) {
                parents.Add(parent);
                parents.AddRange(parent.GetAllParentsRecursive());
            }

            return parents;
        }

        public UpgradeNode GetRoot() {
            if (Parents.Count == 0 || Parents.All(parent => parent.IsBlocked)) {
                return this;
            }

            return Parents.First(parent => !parent.IsBlocked).GetRoot();
        }

        protected abstract void OnUnlock();

        public abstract void OnEnableUpgrade();

        public abstract void OnDisableUpgrade();

        public UpgradeNode GetUpgradeOfType(Type type) {
            foreach (var upgrade in GetAllChildren()) {
                if (upgrade.GetType() == type) {
                    return upgrade;
                }
            }

            return null;
        }

        #endregion
    }
}