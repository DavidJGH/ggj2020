﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Upgrades.Graph
{
    [Serializable]
    public class FlareUpgrade : UpgradeNode
    {
        [SerializeField]
        private Transform flareFirePoint;

        [SerializeField][AssetsOnly]
        private GameObject flarePrefab;

        [SerializeField]
        private float timeBetweenFire = 2f;
        private float timeOfLastFire;

        [SerializeField]
        private float fireForce = 1000f;

        [SerializeField]
        private int flareAmmo = 4; // Static, weil keine Start Method, und ich currentAmmo gleich initialisieren muss
        private int currentAmmo;
        
        private void ShootFlare()
        {  
            if (timeOfLastFire + timeBetweenFire < Time.time && currentAmmo > 0)
            {
                GameObject flare = GameObject.Instantiate(flarePrefab, flareFirePoint.position, flareFirePoint.rotation);
                flare.GetComponent<Rigidbody>().AddForce(flare.transform.forward.normalized * fireForce);
                timeOfLastFire = Time.time;
                currentAmmo--;
            }
        }
        
        protected override void OnUnlock()
        {
        }

        public override void OnEnableUpgrade()
        {
            InputManager.Instance.OnLeftClickDown += ShootFlare;
            currentAmmo = flareAmmo;
        }

        public override void OnDisableUpgrade()
        {
            InputManager.Instance.OnLeftClickDown -= ShootFlare;
        }
    }
}