﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Upgrades.Graph {
    public class ZoomUpgrade : UpgradeNode
    {
        [SerializeField][SceneObjectsOnly]
        private Camera outdoorCamera;
        [SerializeField][SceneObjectsOnly]
        private Camera indoorCamera;

        [SerializeField]
        private float outdoorFovChange = 40f;
        [SerializeField]
        private float indoorFovChange = 40f;
    
        private float zoomedInFovOutdoor;
        private float zoomedOutFovOutdoor;
        private float zoomedInFovIndoor;
        private float zoomedOutFovIndoor;

        public override void OnDisableUpgrade()
        {
            InputManager.Instance.OnRightClickDown -= ZoomIn;
            InputManager.Instance.OnRightClickUp -= ZoomOut;
        }

        public override void OnEnableUpgrade()
        {
            InputManager.Instance.OnRightClickDown += ZoomIn;
            InputManager.Instance.OnRightClickUp += ZoomOut;
        }

        protected override void OnUnlock()
        {
            zoomedOutFovOutdoor = outdoorCamera.fieldOfView;
            zoomedInFovOutdoor = outdoorCamera.fieldOfView - outdoorFovChange;
            zoomedOutFovIndoor = indoorCamera.fieldOfView;
            zoomedInFovIndoor = indoorCamera.fieldOfView - indoorFovChange;
        }

        void ZoomIn()
        {
            outdoorCamera.DOFieldOfView(zoomedInFovOutdoor, 0.3f);
            indoorCamera.DOFieldOfView(zoomedInFovIndoor, 0.3f);
        }

        void ZoomOut()
        {
            outdoorCamera.DOFieldOfView(zoomedOutFovOutdoor, 0.3f);
            indoorCamera.DOFieldOfView(zoomedOutFovIndoor, 0.3f);
        }
    }
}