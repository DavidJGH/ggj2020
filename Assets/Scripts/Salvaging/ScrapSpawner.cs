using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utils;

namespace Salvaging
{
    [RequireComponent(typeof(BoxCollider))]
    public class ScrapSpawner : SerializedMonoBehaviour
    {
        [OdinSerialize] public List<GameObject> ScrapPrefabs { get; set; }

        public BoxCollider SpawnArea { get; private set; }

        private ISet<GameObject> _existingScraps;

        private GameObject RandomScrap => ScrapPrefabs[Random.Range(0, ScrapPrefabs.Count - 1)];

        private void Awake()
        {
            SpawnArea = GetComponent<BoxCollider>();
            _existingScraps = new HashSet<GameObject>();
        }

        [Button]
        public void Spawn(float density) // 0.0005 is a nice value
        {
            ForgetDestroyedScraps();

            var scrapsToSpawn = (int) Mathf.Floor(SpawnArea.bounds.size.Volume() * density);
            var existingScraps = _existingScraps.Count;

            for (var i = 0; i < scrapsToSpawn - existingScraps; i++)
            {
                SpawnScrap();
            }
        }

        private void SpawnScrap()
        {
            _existingScraps.Add(
                DependencyManager.RegisteredInstantiate(RandomScrap, VectorUtils.RandomIn(SpawnArea.bounds), Random.rotation));
        }

        private void ForgetDestroyedScraps()
        {
            foreach (var scrap in _existingScraps.Where(scrap => scrap == null))
            {
                _existingScraps.Remove(scrap);
            }
        }
    }
}