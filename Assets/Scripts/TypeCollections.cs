using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.Damage;
using Upgrades;
using Upgrades.Graph;

namespace DefaultNamespace {
    public class TypeCollections {
        public static IEnumerable<Type> UpgradeTypes() {
            IEnumerable<Type> q = typeof(UpgradeNode).Assembly.GetTypes()
                .Where(x => !x.IsAbstract)                                          // Excludes BaseClass
                .Where(x => !x.IsGenericTypeDefinition)                             // Excludes C1<>
                .Where(x => typeof(UpgradeNode).IsAssignableFrom(x));                 // Excludes classes not inheriting from BaseClass

            return q;
        }
        
        public static IEnumerable<Type> ScalableUpgradesFromUpgradeables(params UpgradeGraphHolder[] upgradeables) {
            if (upgradeables == null) {
                return null;
            }
            List<Type> upgrades = new List<Type>();
            foreach (var upgradeable in upgradeables) {
                
                IEnumerable<Type> scalableUpgradesFromUpgradeables = upgradeable.RootUpgrade.GetAllChildren().Where(upgrade =>
                    upgrade.GetType().GetInterfaces().Contains(typeof(IScaleable))).Select(upgrade => upgrade.GetType());
                
                upgrades.AddRange(scalableUpgradesFromUpgradeables);
            }

            return upgrades;
        }
        
        public static IEnumerable<Type> UpgradesFromUpgradeables(params UpgradeGraphHolder[] upgradeables) {
            if (upgradeables == null) {
                return null;
            }
            List<Type> upgrades = new List<Type>();
            foreach (var upgradeable in upgradeables) {
                IEnumerable<Type> scalableUpgradesFromUpgradeables = upgradeable.RootUpgrade.GetAllChildren().Select(upgrade => upgrade.GetType());
                
                upgrades.AddRange(scalableUpgradesFromUpgradeables);
            }

            return upgrades;
        }
    }
}