﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.DependencyInjection;
using DefaultNamespace.Salvaging;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using UnityEngine;
using Upgrades.Graph;

namespace Upgrades {
    [Serializable]
    public abstract class UpgradeGraphHolder : SerializedMonoBehaviour {
        [SerializeField]
        private UpgradeGraph originalGraph;
        [SerializeField][ReadOnly]
        private UpgradeGraph upgradeGraph;

        public UpgradeNode RootUpgrade => upgradeGraph.rootNode;

        [ResolveDependency] public ISalvageBank SalvageBank { get; set; }

        protected virtual void Awake() {
            upgradeGraph = (UpgradeGraph) originalGraph.Copy();
            
            RootUpgrade.Initialise(transform);   
        }

        public ISet<UpgradeNode> GetPossibleUpgrades()
        {
            return RootUpgrade.GetAllChildren()
                .Where(child => child.IsUnlockable && child.cost < SalvageBank.Balance)
                .ToHashSet();
        }

        private ISet<UpgradeNode> disabledNodes = new HashSet<UpgradeNode>();

        [Button]
        public void DisableUpgrades()
        {
            disabledNodes = new HashSet<UpgradeNode>();
            foreach (var child in RootUpgrade.GetAllChildren().Concat(new []{RootUpgrade}))
            {
                if (child.isUnlocked && !child.IsBlocked)
                {
                    child.OnDisableUpgrade();
                    disabledNodes.Add(child);
                }
            }
        }

        [Button]
        public void EnableUpgrades()
        {
            foreach (var child in disabledNodes)
            {
                child.OnEnableUpgrade();
            }
        }

        public T GetUpgradeOfType<T>() where T : UpgradeNode {
            return GetUpgradeOfType(typeof(T)) as T;
        }

        public UpgradeNode GetUpgradeOfType(Type type)
        {
            var upgrades = new HashSet<UpgradeNode>(RootUpgrade.GetAllChildren()) {RootUpgrade};
            foreach (var upgrade in upgrades)
            {
                if (upgrade.GetType() == type)
                {
                    return upgrade;
                }
            }

            return null;
        }
    }
}