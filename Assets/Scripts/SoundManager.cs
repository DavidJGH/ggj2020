using System.Collections;
using DefaultNamespace.Damage;
using DefaultNamespace.DependencyInjection;
using DG.Tweening;
using UnityEngine;
using Upgrades.Movement;

namespace DefaultNamespace
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private MovementUpgradeGraphHolder movementUpgradeGraphHolder;
        
        [ResolveDependency]
        public ICollisionDamager CollisionDamager { get; set; }

        public AudioSource AlarmSource;
        public AudioSource MusicSource;
        public AudioSource DriveSource;
        public AudioSource EngineStartSource;
        public AudioSource EngineRunningSource;
        public AudioSource IdlingSource;
        public AudioSource CollisionSource;
        public AudioSource SonarSource;
        public AudioSource RadarSource;
        public AudioSource EnginePowerDownSource;
        public AudioSource DepthAlarmSource;
        public AudioSource SuspenseSource;

        public static SoundManager Instance = null;


        [SerializeField] private AudioClip driveSound;
        [SerializeField] private AudioClip engineStartSound;
        [SerializeField] private AudioClip engineRunningSound;
        [SerializeField] private AudioClip suspense1;
        [SerializeField] private AudioClip eerie1;
        [SerializeField] private AudioClip eerie2;
        [SerializeField] private AudioClip idlingSound;
        [SerializeField] private AudioClip hullDamage1;
        [SerializeField] private AudioClip impact1;
        [SerializeField] private AudioClip impact2;
        [SerializeField] private AudioClip sonar;
        [SerializeField] private AudioClip radar;
        [SerializeField] private AudioClip enginePowerDownSound;
        [SerializeField] private AudioClip depthAlarmSound;

        [SerializeField]
        private AnimationCurve pitchCurve;
        
        private float currentPitch = 0f;
        
        private bool doSuspense;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            StartCoroutine(PlayEngineStart());
            StartCoroutine(SuspenseRoutine());
            doSuspense = true;
            // movementUpgradeGraphHolder.OnMovement += vec =>
            // {
            //     currentPitch = pitchCurve.Evaluate(vec.magnitude);
            //     UpdateDriveSound(currentPitch, 1f);
            // };

            CollisionDamager.OnCollision += PlayCollision;
        }

        public void PlayDrive()
        {
            DriveSource.clip = driveSound;
            DriveSource.volume = 0.7f;
            DriveSource.outputAudioMixerGroup.audioMixer.SetFloat("Pitch", 0f);
            DriveSource.Play();
        }
        
        public void UpdateDriveSound(float pitch, float speed)
        {
            DriveSource.outputAudioMixerGroup.audioMixer.SetFloat("Pitch", pitch);
        }

        public IEnumerator PlayEngineStart()
        {
            EngineStartSource.clip = engineStartSound;
            EngineStartSource.Play();
            yield return new WaitForSeconds(0.5f);
            EngineStartSource.DOFade(0f, 3f);
            DriveSource.volume = 0f;
            PlayDrive();
            DriveSource.DOFade(1f, 1f);
        }

        public void PlayMusic(AudioClip clip)
        {
            MusicSource.clip = clip;
            MusicSource.Play();
        }

        public void PlayIdling()
        {
            IdlingSource.clip = idlingSound;
            IdlingSource.Play();
        }

        public void PlayCollision(float impact)
        {
            switch (Random.Range(0,2))
            {
                case 0:
                    CollisionSource.clip = impact1;
                    break;
                case 1:
                    CollisionSource.clip = impact2;
                    break;
            }

            CollisionSource.volume = impact;

            CollisionSource.pitch = Random.Range(0.8f, 1.2f);
            CollisionSource.Play();
        }


        public void StartSuspenseRoutine() {
            doSuspense = true;
        }

        public void StopSuspenseRoutine() {
            doSuspense = false;
        }

        
        IEnumerator SuspenseRoutine() {
            while (true) {
                switch (UnityEngine.Random.Range(0,3))
                {
                    case 0:
                        SuspenseSource.clip = suspense1;
                        break;
                    case 1:
                        SuspenseSource.clip = eerie1;
                        break;
                    case 2:
                        SuspenseSource.clip = eerie2;
                        break;
                }

                SuspenseSource.Play();
                
                yield return new WaitForSeconds(SuspenseSource.clip.length + UnityEngine.Random.Range(13f,40f));
                yield return new WaitUntil(() => doSuspense);
            }
        }

        public void PlaySonar()
        {
            SonarSource.clip = sonar;
            SonarSource.Play();
        }

        public void PlayRadar()
        {
            RadarSource.clip = radar;
            RadarSource.Play();
        }

        public IEnumerator PlayEnginePowerDown()
        {
            EnginePowerDownSource.clip = enginePowerDownSound;
            EnginePowerDownSource.Play();
            yield return new WaitUntil(() => !EnginePowerDownSource.isPlaying);
            PlayIdling();
        }

        public void PlayDepthAlarm()
        {
            DepthAlarmSource.clip = depthAlarmSound;
            DepthAlarmSource.Play();
        }
    }
}