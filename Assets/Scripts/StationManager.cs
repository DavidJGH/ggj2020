﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class StationManager : MonoBehaviour
{
    public static StationManager Instance = null;
    
    public List<Station> stations;

    public int activeStation;

    [SerializeField]
    private Transform player;

    [SerializeField]
    private float movementSpeed;

    [SerializeField]
    private float angularTransitionTime;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
        player.localPosition = stations[activeStation].transform.localPosition;
        player.localRotation = stations[activeStation].transform.localRotation;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
            MoveToPrevious();
        if(Input.GetKeyDown(KeyCode.RightArrow))
            MoveToNext();
    }

    void MoveToNext()
    {
        MoveTo(activeStation == stations.Count-1 ? 0 : activeStation + 1);
    }
    
    void MoveToPrevious()
    {
        MoveTo(activeStation == 0 ? stations.Count-1 : activeStation-1);
    }

    private Tween _moveTweener;
    private Tween _rotateTweener;
    void MoveTo(int i)
    {
        stations[activeStation].OnExitStation();
        stations[i].OnEnterStation();
        activeStation = i;
        _moveTweener.Kill();
        _rotateTweener.Kill();
        _moveTweener = player.DOLocalMove(stations[activeStation].transform.localPosition, Vector3.Distance(transform.localPosition, stations[activeStation].transform.localPosition)/movementSpeed);
        _rotateTweener = player.DOLocalRotate(stations[activeStation].transform.localRotation.eulerAngles,
            angularTransitionTime);
    }

    private void OnDrawGizmos()
    {
        if(stations is null)
            return;
        for (int i = 0; i < stations.Count; i++)
        {
            if(stations[i].transform is null)
                return;
            Gizmos.color = activeStation == i ? Color.white : Color.black;
            Gizmos.DrawSphere(stations[i].transform.position, .1f);
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(stations[i].transform.position, stations[i].transform.position + stations[i].transform.forward);
        }
        Gizmos.color = Color.green;
        for (int i = 0; i < stations.Count; i++)
        {
            Gizmos.DrawLine(stations[i].transform.position, stations[i == stations.Count-1 ? 0 : i+1].transform.position);
        }
    }
}

[Serializable]
public class Station
{
    public Transform transform;
    public event Action EnterStation;
    public event Action ExitStation;

    public bool freeMouse = false;

    public void OnEnterStation()
    {
        if (freeMouse) {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        } else {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        EnterStation?.Invoke();
    }

    public void OnExitStation()
    {
        ExitStation?.Invoke();
    }
}