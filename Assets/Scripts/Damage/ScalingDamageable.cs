using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace DefaultNamespace.Damage
{
    [Serializable]
    public class ScalingDamageable : UpgradeGroupDamageable
    {
        
        [ValueDropdown(nameof(DropDownTypes))]
        [OdinSerialize] public List<Type> UpgradesToDisable { get; set; }

        public override IEnumerable<Type> DropDownTypes() {
            return TypeCollections.ScalableUpgradesFromUpgradeables(DamageableUpgradeables.ToArray());
        }
        
        public override void OnDamaged(float hitValue)
        {
            UpdateTargetValues();
        }

        public override void OnRepaired(float repairValue)
        {
            UpdateTargetValues();
        }

        private void UpdateTargetValues()
        {
            foreach (var scaleable in GetTargetUpgrades().Select(upgrade => upgrade as IScaleable).Where(upgrade => upgrade != null))
            {
                scaleable.ScaleValues(Health);
            }
        }
    }
}