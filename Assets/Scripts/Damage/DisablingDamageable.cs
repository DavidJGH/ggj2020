using System;
using System.Collections.Generic;
using Sirenix.Serialization;

namespace DefaultNamespace.Damage
{
    [Serializable]
    public class DisablingDamageable : UpgradeGroupDamageable
    {
        [OdinSerialize] public float EnableThreshold { get; set; }

        public override IEnumerable<Type> DropDownTypes() {
            return TypeCollections.UpgradesFromUpgradeables(DamageableUpgradeables.ToArray());
        }

        public override void OnDamaged(float hit)
        {
            if (Health >= EnableThreshold) return;

            foreach (var upgrade in GetTargetUpgrades())
            {
                if (upgrade != null) {
                    upgrade.OnDisableUpgrade();
                }
            }
        }

        public override void OnRepaired(float repairValue)
        {
            if (Health < EnableThreshold) return;
            
            foreach (var upgrade in GetTargetUpgrades())
            {
                upgrade.OnEnableUpgrade();
            }
        }
    }
}