namespace Upgrades.Graph.Movement
{
    public interface IMovementController
    {
        float MaxVelocity { get; set; }
        float DampingCoefficient { get; set; }
    }
}