using DefaultNamespace.DependencyInjection;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;

namespace DefaultNamespace.Salvaging
{
    [RequireComponent(typeof(Collider), typeof(Renderer))]
    public class Salvageable : SerializedMonoBehaviour
    {
        [OdinSerialize] public int Value { get; set; }

        [ResolveDependency]
        public ISalvageBank SalvageBank { get; set; }

        private Collider _collider;
        private Renderer _renderer;
        
        public UnityEvent onPickup;

        private void Awake() {
            _collider = GetComponent<Collider>();
            _renderer = GetComponent<Renderer>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            SalvageBank.Balance += Value;
            onPickup.Invoke();
            
            _renderer.enabled = false;
            _collider.enabled = false;
            
            Destroy(gameObject, 3f);
        }
    }
}