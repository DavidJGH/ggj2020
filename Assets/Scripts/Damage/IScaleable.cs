namespace DefaultNamespace.Damage
{
    public interface IScaleable
    {
        void ScaleValues(float coefficient);
    }
}