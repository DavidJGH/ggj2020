﻿using UnityEngine;
using XNode;

namespace Upgrades.Graph {
    public abstract class UpDownNode : Node{
        [InputAttribute, HideInInspector] public UpgradeNode input;
        [OutputAttribute, HideInInspector] public UpgradeNode output;
    }
}