﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XNode;

namespace Upgrades.Graph.Logic {
    public class MultipleParentOperator : UpDownNode {
        [LabelWidth(130f)]
        public AllowUnlockType allowUnlockType;

        private UpgradeNode childNode;

        public enum AllowUnlockType {
            Any,
            All
        }

        private void OnValidate() {
            SetUnlockMode();
        }

        public override void OnCreateConnection(NodePort from, NodePort to) {
            SetUnlockMode();
            base.OnCreateConnection(@from, to);
        }

        private void SetUnlockMode() {
            if (childNode == null) {
                childNode = GetInputPort(nameof(input)).GetInputValue<UpgradeNode>();
            }

            if (childNode != null) {
                childNode.unlockModeIsAny = allowUnlockType == AllowUnlockType.Any;
            }
        }

        public override void OnRemoveConnection(NodePort port) {
            childNode = null;
            base.OnRemoveConnection(port);
        }

        public override object GetValue(NodePort port) {
            if (port.fieldName == nameof(output)) {
                return GetInputPort(nameof(input)).GetInputValue<UpgradeNode>();
            }

            return base.GetValue(port);
        }
    }
}