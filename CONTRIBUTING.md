# Contributing to Supmarine

Work in *short-lived feature branches*:

For every code change, create a new branch in Git. 

If the change contains a new feature, name the branch `feature/your-new-feature`.

If the change contains a fix of an existing feature, name the branch
`bugfix/thing-you-fixed`.

When your feature is ready to be merged, create a merge-request and let it be
reviewed by a fellow developer.

If required, create a small demo scene to be used in the review.

Before changing the main scene, please communicate this to fellow developers so
we get as little merge conflicts as possible.

Try to keep feature branches small and self-contained. When developing a large
new feature, consider splitting it into multiple incremental feature branches.
Reviewing 5 branches with 5 changes each is easier than reviewing one branch
with 30 changes.

We have nothing to lose but our tech debt. - Philipp Ploder, 2020-03-22

## Git emergency help line

- Andreas
- Philipp
- Raphael
