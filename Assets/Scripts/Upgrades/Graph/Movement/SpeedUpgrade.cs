using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using UnityEngine;

namespace Upgrades.Graph.Movement {
    public class SpeedUpgrade : UpgradeNode {
        [SerializeField]
        public float speedMultiplier;

        private HashSet<IMovementController> movementControllers;

        protected override void OnUnlock() {
            movementControllers = GetAllParents()
                .Select(parent => parent as IMovementController)
                .Where(ctl => ctl != null).ToHashSet();
        }

        public override void OnEnableUpgrade() {
            foreach (var controller in movementControllers) {
                controller.MaxVelocity *= speedMultiplier;
                controller.DampingCoefficient *= speedMultiplier;
            }
        }

        public override void OnDisableUpgrade() {
            foreach (var controller in movementControllers) {
                controller.MaxVelocity /= speedMultiplier;
                controller.DampingCoefficient /= speedMultiplier;
            }
        }
    }
}