﻿using UnityEngine;
using XNodeEditor;

namespace Upgrades.Graph.Editor {
    [CustomNodeGraphEditor(typeof(UpgradeGraph))]
    public class UpgradeableGraphEditor : NodeGraphEditor {
        private UpgradeGraph upgradeGraph;

        private Texture2D secondaryTexture;
        
        public override string GetNodeMenuName(System.Type type) {
            if (type.Namespace?.Contains("Upgrades.Graph") ?? false) {
                return base.GetNodeMenuName(type).Replace("Upgrades/Graph/", "");
            }

            return base.GetNodeMenuName(type);
        }

        public override Texture2D GetGridTexture() {
            if (upgradeGraph == null) {
                upgradeGraph = (UpgradeGraph) target;
            }

            if (upgradeGraph != null && upgradeGraph.gridBackGround != null) {
                return upgradeGraph.gridBackGround;
            }

            return base.GetGridTexture();
        }

        
        
        public override Texture2D GetSecondaryGridTexture() {
            if (secondaryTexture == null) {
                secondaryTexture = new Texture2D(0,0);     }
            return secondaryTexture;
        }
    }
}