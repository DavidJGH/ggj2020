﻿Shader "Custom/SonarRadarShader"
{
    Properties
    {
        [HDR]_BColor ("Background Color", Color) = (1,1,1,1)
        [HDR]_Color ("Scanner Color", Color) = (1,1,1,1)
        [HDR]_DColor ("Dot Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Scale ("Circles Scale", Range(0,1000)) = 100.0
        _Thickness ("Circle Thickness", Range(0,1)) = .5
        _OThickness ("Outer Circle Thickness", Range(0,1)) = .5
        _NLines ("Number of Lines", Int) = 5
        _LThickness ("Line Thickness", Range(0,1)) = .5
        _CDot ("Center Dot Size", Range(0,1)) = .5
        _Dot ("Dots Size", Range(0,1)) = .5
        _ScanlineSpeed ("Scanline Speed", Range(0,5)) = 1
        _ScanlineWidth ("Scanline Width", Range(0,.025)) = .001
        _Scanline ("Scanline Trail Opacity", Range(0,1)) = .5
        _BackgroundOpacity ("Background Opacity", Range(0,1)) = .2
        _BackgroundFalloff ("Background Faloff", Range(0,1)) = 1
        _Cutoff ("Cutoff Distance", Range(0,1.0)) = .5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
        
        #define maxDots 10

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        half _Scale;
        half _Thickness;
        half _OThickness;
        half _Cutoff;
        int _NLines;
        half _LThickness;
        half _CDot;
        half _Dot;
        half _Scanline;
        half _ScanlineWidth;
        half _ScanlineSpeed;
        half _BackgroundOpacity;
        half _BackgroundFalloff;
        fixed4 _Color;
        fixed4 _BColor;
        fixed4 _DColor;
        
        uniform float2 _Dots[maxDots];

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float time = _Time[1] * _ScanlineSpeed;
            
            float2 uv = IN.uv_MainTex;
            
            float dist = distance(uv,float2(0.5, 0.5));
            
            float centerDot = step(dist, _CDot);
            float circles = step((sin(dist*_Scale)+1)/2, _Thickness);
            
            float outerCircle = 1-step((sin(dist/_Cutoff)+1)/2,1-_OThickness/2);
            
            float background = (_BackgroundFalloff * dist + (1-_BackgroundFalloff)) * _BackgroundOpacity;
            
            float2 centerVector = IN.uv_MainTex-float2(.5,.5);
            float2 normCenterVector = centerVector / sqrt(centerVector.x * centerVector.x + centerVector.y * centerVector.y);
            
            float lines = step( 
                ((cos(
                        acos(normCenterVector.y)*_NLines
                    )+1
                )/2),_LThickness);
            
            float cutoff = step(dist, _Cutoff);
            
            float2 pointer = float2(sin(time),cos(time));
            float2 rpointer = float2(sin(time+1.57),cos(time+1.57));
            //float2 r2pointer = float2(sin(time-1.57),cos(time-1.57));
            
            //float scanlineTrail = 0.0; 
            float scanlineRender = 0.0;
            float scanline = 1.0;
    
            if(dot(rpointer, normCenterVector) < 0.0){
            /*
               scanlineTrail =   
                 ((cos(
                         acos(normCenterVector.x * pointer.x + normCenterVector.y * pointer.y)
                     )+1.0
                 )/3.0);*/
                
                
               scanlineRender =  
                ((cos(
                        acos(normCenterVector.x * pointer.x + normCenterVector.y * pointer.y)
                    )+1.0
                )/2.0);
            }
            
            if(dot(pointer, normCenterVector) > 0.0){
    	        scanline = sin(time+1.57)*(uv.x-.5) + cos(time+1.57) * (uv.y-.5);
            }
            
            scanline = step(abs(scanline), _ScanlineWidth);
            
            float drawRadar = min(background+lines+circles+outerCircle+centerDot+scanlineRender*_Scanline+scanline,cutoff);
            
            float drawDot = 0;
            
            for(int i=0; i<maxDots; i++){
                if(_Dots[i].x == 0 && _Dots[i].y == 0)
                    continue;
                float dotDist = distance(uv,_Dots[i]+.5);
                drawDot = min(drawDot + step(dotDist, _Dot) * cutoff, 1);
            }
            
            drawDot *= scanlineRender;
            
            float3 color = _Color * drawRadar + _BColor * (1-drawRadar);
            color = _DColor * drawDot + color * (1-drawDot);
            
            o.Albedo = color;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
