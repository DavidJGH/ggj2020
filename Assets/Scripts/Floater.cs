﻿using UnityEngine;

public class Floater : MonoBehaviour
{
    [SerializeField]
    private float impact;

    private Rigidbody _rb;

    private Vector3 position;
    
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        position = _rb.position;
    }

    void Update()
    {
        _rb.position = position + (new Vector3(Mathf.Sin(Mathf.PerlinNoise(position.x + Time.time, position.y)), Mathf.Sin(Mathf.PerlinNoise(position.y + Time.time, position.z)), Mathf.Sin(Mathf.PerlinNoise(position.z + Time.time, position.x)))*impact);
    }
}
