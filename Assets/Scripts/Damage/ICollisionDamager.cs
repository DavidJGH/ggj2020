using System;

namespace DefaultNamespace.Damage {
    public interface ICollisionDamager {
        event Action<float> OnCollision;
    }
}