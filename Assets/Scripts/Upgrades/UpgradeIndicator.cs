using System;
using System.Collections.Generic;
using DefaultNamespace;
using DefaultNamespace.DependencyInjection;
using DefaultNamespace.Salvaging;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Upgrades;
using Upgrades.Graph;

namespace Upgrades {
    public class UpgradeIndicator : SerializedMonoBehaviour, IClickable {
        [OdinSerialize] private UpgradeGraphHolder upgradeGraphHolder;
        [ResolveDependency]
        public ISalvageBank _salvageBank { get; set; }
        [SerializeField] private TMP_Text text;

        [ValueDropdown(nameof(UpgradeTypes))] [OdinSerialize]
        private Type _upgradeType;

        [SerializeField] private Renderer buttonRenderer;
        [SerializeField] private int materialIndex;

        [ColorUsage(true, true)] [SerializeField]
        private Color unlockedColor = Color.green;

        public UnityEvent onUnlock;

        private IEnumerable<Type> UpgradeTypes() {
            return TypeCollections.UpgradeTypes();
        }

        private UpgradeNode _upgrade;

        private bool _unlockable;

        private void Start() {
            _upgrade = upgradeGraphHolder.GetUpgradeOfType(_upgradeType);

            _salvageBank.OnBalanceChange += balance => {
                _unlockable = balance >= _upgrade.cost && _upgrade.IsUnlockable;
                text.color = _unlockable ? Color.green : Color.red;
            };

            text.text = _upgrade.cost.ToString();
        }

        void IClickable.OnClick() {
            if (_unlockable && !_upgrade.isUnlocked) {
                _upgrade.Unlock();
                onUnlock.Invoke();
                _salvageBank.Balance -= _upgrade.cost;
                text.text = "";
                buttonRenderer.materials[materialIndex].SetColor("_EmissionColor", unlockedColor);
            }
        }
    }
}