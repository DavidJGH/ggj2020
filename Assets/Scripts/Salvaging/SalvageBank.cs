using System;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using TMPro;
using UnityEngine;

namespace DefaultNamespace.Salvaging
{
    public class SalvageBank : SerializedMonoBehaviour, ISalvageBank {
        [OdinSerialize]
        private int _balance;

        [SerializeField] private TMP_Text _text;


        private void Start() {
            if (_text != null) {
                OnBalanceChange += i => _text.text = i.ToString();
            }
        }

        public int Balance
        {
            get => _balance;
            set
            {
                _balance = value;
                OnBalanceChange?.Invoke(_balance);
            }
        }

        public event Action<int> OnBalanceChange;

        [Button(ButtonSizes.Large)]
        public void TestAddScrap() {
            Balance += 1;
        }
    }
}