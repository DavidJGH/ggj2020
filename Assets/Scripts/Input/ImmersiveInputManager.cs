using UnityEngine;

namespace DefaultNamespace
{
    [RequireComponent(typeof(Camera))]
    public class ImmersiveInputManager : MonoBehaviour
    {
        //[SerializeField] private StandaloneInputModuleV2 inputModule;

        private Camera _camera;

        private void Awake() {
            _camera = GetComponent<Camera>();
        }

        private void Update() {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var hit,500f)) {
                    hit.collider.gameObject.GetComponent<IClickable>()?.OnClick();
                }
            }
        }
    }
}