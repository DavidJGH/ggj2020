﻿using System.Collections.Generic;
using DefaultNamespace.Damage;
using UnityEngine;
using Upgrades.Graph.Movement;

namespace Upgrades.Graph
{
    public class LightUpgrade : UpgradeNode, IScaleable
    {
        public float LightStrength
        {
            get => _lightStrength * _scalingCoefficient;
            set => _lightStrength = value;
        }

        private HashSet<IMovementController> _movementControllers;
        
        [SerializeField]
        private float _lightStrength;
        [SerializeField]
        private float _scalingCoefficient = 1f;
        
        //TODO: make an event the lights listen for instead
        [SerializeField]
        private List<Light> _lights = new List<Light>();

        protected override void OnUnlock()
        {
            
        }

        public override void OnEnableUpgrade()
        {
            _lights.ForEach(e => e.intensity = LightStrength);
        }

        public override void OnDisableUpgrade()
        {
            _lights.ForEach(e => e.intensity = 0);
        }

        public void ScaleValues(float coefficient)
        {
            _scalingCoefficient = coefficient;
            _lights.ForEach(e => e.intensity = LightStrength);
        }
    }
}