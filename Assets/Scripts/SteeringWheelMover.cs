﻿using UnityEngine;
using Upgrades.Graph.Movement;
using Upgrades.Movement;

[RequireComponent(typeof(MovementUpgradeGraphHolder))]
public class SteeringWheelMover : MonoBehaviour {
    [SerializeField] private Transform steeringWheel;
    [SerializeField] private Transform pitchLever;

    private Quaternion pitchNeutralPos;
    [SerializeField] private float pitchStrength = 1f;
    [SerializeField] private float steerStrength = 1f;

    private MovementUpgradeGraphHolder movementUpgradeGraphHolder;

    private void Awake() {
        movementUpgradeGraphHolder = GetComponent<MovementUpgradeGraphHolder>();
    }

    private float lastFrameValue;
    private void Start() {
        LookaroundUpgrade lookAround = movementUpgradeGraphHolder.GetUpgradeOfType<LookaroundUpgrade>();
        MoveForwardUpgrade moveForwardUpgrade = movementUpgradeGraphHolder.GetUpgradeOfType<MoveForwardUpgrade>();
        if (lookAround != null) {
            lookAround.OnRotation += OnMovement;
        } else {
            Debug.Log("LookAround not found not updating steering visuals");
        }

        if (moveForwardUpgrade != null) {
            
            InputManager.Instance.OnZMove += f => {
                float target = Mathf.Lerp(-140f, -55f, (f+1)/2f);
                float value = Mathf.Lerp(lastFrameValue, target, 0.05f);
                pitchLever.localRotation = Quaternion.Euler(value, 0f,0f);
                lastFrameValue = value;
            };
        } else {
            Debug.Log("LookAround not found not updating steering visuals");
        }

    }

    private void OnMovement(Vector2 input) {
        steeringWheel.Rotate(Vector3.forward, input.x*steerStrength, Space.Self);
    }
}
