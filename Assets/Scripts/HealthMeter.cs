﻿using System.Linq;
using DefaultNamespace.Damage;
using UnityEngine;
using UnityEngine.UI;

public class HealthMeter : MonoBehaviour {
    [SerializeField] private DamageManager damageManager;
    [SerializeField] private Slider _slider;

    private void FixedUpdate() {
        _slider.value = damageManager.Damageables.FirstOrDefault().Health;
    }
}
