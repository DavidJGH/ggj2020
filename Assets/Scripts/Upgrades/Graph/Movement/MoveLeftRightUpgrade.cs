using System;
using DefaultNamespace.Damage;
using Sirenix.Serialization;
using UnityEngine;
using Upgrades.Graph;
using Upgrades.Movement;
using Vector3 = UnityEngine.Vector3;

namespace Upgrades.Graph.Movement {
    [Serializable]
    public class MoveLeftRightUpgrade : UpgradeNode, IMovementController, IScaleable {
        public float MaxVelocity {
            get => maxVelocity * scalingCoefficient;
            set => maxVelocity = value;
        }

        public float DampingCoefficient {
            get => dampingCoefficient * scalingCoefficient;
            set => dampingCoefficient = value;
        }

        [SerializeField]
        private float maxVelocity = 4f;
        [SerializeField]
        private float dampingCoefficient = 0.05f;
        [SerializeField]
        private float scalingCoefficient = 1;

        private Rigidbody _rigidbody;

        protected override void OnUnlock() {
            _rigidbody = Transform.GetComponent<Rigidbody>();
        }

        public override void OnEnableUpgrade() {
            InputManager.Instance.OnXMove += MoveLeftOrRight;
        }

        private void MoveLeftOrRight(float input) {
            if (Math.Abs(input) < float.Epsilon) return;

            var currentVelocity = _rigidbody.velocity;
            var targetVelocity = Transform.right * (MaxVelocity * input);
            _rigidbody.velocity = Vector3.Lerp(currentVelocity, targetVelocity, DampingCoefficient);

            OnMoveLeftOrRight?.Invoke(Mathf.Abs(Transform.InverseTransformVector(_rigidbody.velocity).x));
        }

        public override void OnDisableUpgrade() {
            InputManager.Instance.OnXMove -= MoveLeftOrRight;
        }

        public event Action<float> OnMoveLeftOrRight;

        void IScaleable.ScaleValues(float coefficient) {
            scalingCoefficient = coefficient;
        }
    }
}