using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Upgrades;
using Upgrades.Graph;

namespace DefaultNamespace.Damage {
    [Serializable]
    public abstract class UpgradeGroupDamageable : IDamageable {
        [ShowInInspector] public float Health { get; set; }

        [OdinSerialize] public List<UpgradeGraphHolder> DamageableUpgradeables { get; set; }

        [ValueDropdown(nameof(DropDownTypes))]
        [OdinSerialize]
        public List<Type> UpgradesToDisable { get; set; }

        public abstract IEnumerable<Type> DropDownTypes();

        protected IEnumerable<UpgradeNode> GetTargetUpgrades() {
            foreach (UpgradeGraphHolder upgradeable in DamageableUpgradeables) {
                foreach (var upgradeType in UpgradesToDisable) {
                    yield return upgradeable.GetUpgradeOfType(upgradeType);
                }
            }
        }

        public abstract void OnDamaged(float hitValue);
        public abstract void OnRepaired(float repairValue);
    }
}