﻿using UnityEngine;

namespace Upgrades.Graph.Sonar {
    [RequireComponent(typeof(SonarShadarManager))]
    public class SonarEntityTracker : MonoBehaviour
    {
        private SonarShadarManager _shaderManager;

        [SerializeField]
        private LayerMask trackingMask;

        private bool _enabled;

        private void Awake()
        {
            Debug.Log("eee");
            _shaderManager = GetComponent<SonarShadarManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if(!_enabled) return;
        
            if (((1<<other.gameObject.layer) & trackingMask) != 0)
            {
                _shaderManager.dots.Add(other.transform);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if(!_enabled) return;
        
            _shaderManager.dots.Remove(other.transform);
        }

        private void OnEnable()
        {
            _enabled = true;
        }

        private void OnDisable()
        {
            _enabled = false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, transform.lossyScale.x/2f);
        }
    }
}