using System;
using Sirenix.Serialization;
using UnityEngine;

namespace Upgrades.Graph.Movement {
    public class LookaroundUpgrade : UpgradeNode {
        [OdinSerialize] public float maxAngularVelocity = 30f;
        [OdinSerialize] public float dampingCoefficient = 0.02f;
        [OdinSerialize] public float clampValue = 5000;
        [OdinSerialize] public float inputDampingCoefficient = 0.05f;

        public event Action<Vector2> OnRotation;
        
        private Rigidbody _rigidbody;

        protected override void OnUnlock() {
            _rigidbody = Transform.GetComponent<Rigidbody>();
        }

        public override void OnEnableUpgrade() {
            InputManager.Instance.OnMouseMove += MoveRotation;
        }

        float yaw;
        float pitch;

        private float xInput;
        private float yInput;

        private void MoveRotation(Vector2 value) {
            
            xInput = Mathf.Lerp(xInput, value.x, inputDampingCoefficient);
            yInput = Mathf.Lerp(yInput, value.y, inputDampingCoefficient);

            pitch = Mathf.Lerp(pitch, pitch - yInput * maxAngularVelocity, dampingCoefficient);
            yaw = Mathf.Lerp(yaw, yaw + xInput * maxAngularVelocity, dampingCoefficient);

            OnRotation?.Invoke(new Vector2(xInput, yInput));

            _rigidbody.rotation = Quaternion.Euler(new Vector3(pitch, yaw, 0f));
        }

        public override void OnDisableUpgrade() {
            InputManager.Instance.OnMouseMove -= MoveRotation;
        }
    }
}