﻿using System;
using DefaultNamespace.Damage;
using UnityEngine;

namespace Upgrades.Graph.Movement {
    public class RiseSinkUpgrade : UpgradeNode,IMovementController, IScaleable{
        public float MaxVelocity
        {
            get => maxVelocity * scalingCoefficient;
            set => maxVelocity = value;
        }
        
        public float DampingCoefficient
        {
            get => dampingCoefficient * scalingCoefficient;
            set => dampingCoefficient = value;
        }
        
        [SerializeField]
        private float maxVelocity = 3f;
        [SerializeField]
        private float dampingCoefficient = 0.05f;
        [SerializeField]
        private float scalingCoefficient = 1;

        private Rigidbody rigidbody;

        protected override void OnUnlock()
        {
            rigidbody = Transform.GetComponent<Rigidbody>();
        }

        public override void OnEnableUpgrade()
        {
            InputManager.Instance.OnYMove += RiseOrSink;
        }

        private void RiseOrSink(float value)
        {
            if(Math.Abs(value) < float.Epsilon) return;
            
            var currentVelocity = rigidbody.velocity;
            var targetVelocity = Transform.up * (MaxVelocity * value);
            rigidbody.velocity = Vector3.Lerp(currentVelocity, targetVelocity, DampingCoefficient);
            
            OnRiseOrSink?.Invoke(Mathf.Abs(Transform.InverseTransformVector(rigidbody.velocity).y));
        }

        public override void OnDisableUpgrade()
        {
            InputManager.Instance.OnYMove -= RiseOrSink;
        }

        public event Action<float> OnRiseOrSink;

        public void ScaleValues(float coefficient)
        {
            scalingCoefficient = coefficient;
        }
    }
}