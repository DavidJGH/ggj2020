﻿using System.Linq;
using UnityEngine;
using XNode;

namespace Upgrades.Graph {
    [CreateAssetMenu]
    public class UpgradeGraph : NodeGraph {
        public Texture2D gridBackGround;
        public UpgradeNode rootNode;

        public override NodeGraph Copy() {
            // Instantiate a new nodegraph instance
            UpgradeGraph graph = Instantiate(this);
            // Instantiate all nodes inside the graph
            for (int i = 0; i < nodes.Count; i++) {
                if (nodes[i] == null) continue;
                Node.graphHotfix = graph;
                UpgradeNode currentOriginalUpgradeNode = nodes[i] as UpgradeNode;
                
                Node currentCopyNode = Instantiate(nodes[i]) as Node;
                currentCopyNode.graph = graph;
                if (currentOriginalUpgradeNode != null && currentOriginalUpgradeNode == rootNode) {
                    graph.rootNode = (UpgradeNode) currentCopyNode;
                }
                graph.nodes[i] = currentCopyNode;
            }

            // Redirect all connections
            for (int i = 0; i < graph.nodes.Count; i++) {
                if (graph.nodes[i] == null) continue;
                foreach (NodePort port in graph.nodes[i].Ports) {
                    port.Redirect(nodes, graph.nodes);
                }
            }

            return graph;
        }
    }
}