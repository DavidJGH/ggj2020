﻿using System.Linq;
using DefaultNamespace;
using DefaultNamespace.Damage;
using DefaultNamespace.Salvaging;
using TMPro;
using UnityEngine;

public class RepairSubmarineButton : MonoBehaviour, IClickable {
    [SerializeField] private SalvageBank salvageBank;
    [SerializeField] private DamageManager damageManager;
    private int cost;
    [SerializeField] private TMP_Text text;

    private bool canRepair;

    private void Start() {
        salvageBank.OnBalanceChange += i => {
            canRepair = i>=1;
            text.color = canRepair ? Color.green : Color.red;
        };
    }


    public void OnClick() {
        if (canRepair && damageManager.Damageables.First().Health<1f) {
            damageManager.Repair(damageManager.Damageables.First(),0.2f);
            salvageBank.Balance -= 1;
        }
    }
}
