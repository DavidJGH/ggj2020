using System;
using DefaultNamespace.Damage;
using UnityEngine;
using Upgrades.Movement;

namespace Upgrades.Graph.Movement {
    [Serializable]
    public class MoveForwardUpgrade : UpgradeNode, IMovementController, IScaleable {
        public float MaxVelocity {
            get => maxVelocity * scalingCoefficient;
            set => maxVelocity = value;
        }

        public float DampingCoefficient {
            get => dampingCoefficient * scalingCoefficient;
            set => dampingCoefficient = value;
        }

        [SerializeField]
        private float maxVelocity = 5f;
        [SerializeField]
        private float dampingCoefficient = 0.05f;
        [SerializeField]
        private float scalingCoefficient = 1f;

        private Rigidbody _rigidbody;

        protected override void OnUnlock() {
            _rigidbody = Transform.GetComponent<Rigidbody>();
        }

        public override void OnEnableUpgrade() {
            InputManager.Instance.OnZMove += MoveForward;
        }

        private void MoveForward(float value) {
            if (value < 0) return;

            Vector3 currentVelocity = _rigidbody.velocity;
            Vector3 targetVelocity = Transform.forward * (MaxVelocity * value);
            _rigidbody.velocity = Vector3.Lerp(currentVelocity, targetVelocity, DampingCoefficient);

            OnMoveForward?.Invoke(Transform.InverseTransformVector(_rigidbody.velocity).z);
        }

        public override void OnDisableUpgrade() {
            InputManager.Instance.OnZMove -= MoveForward;
        }

        public event Action<float> OnMoveForward;

        void IScaleable.ScaleValues(float coefficient) {
            scalingCoefficient = coefficient;
        }
    }
}