﻿using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Upgrades.Graph.Sonar {
    public class SonarShadarManager : MonoBehaviour
    {
        [SerializeField]
        private Material sonar;
        [SerializeField]
        private Transform origin;
    
        public List<Transform> dots = new List<Transform>();
        [SerializeField]
        private float relativeRadius = 10;

        private static readonly int Dots = Shader.PropertyToID("_Dots");
    
        private float _scanLineSpeed;
    
        Vector4[] _positionArray = new Vector4[10];

        void Start()
        {
            _scanLineSpeed = sonar.GetFloat("_ScanlineSpeed");
        }

        void Update()
        {
            float time = Time.time * _scanLineSpeed;
            Vector2 pointer = new Vector2(Mathf.Sin(time),Mathf.Cos(time));
            for (int i = 0; i < dots.Count; i++)
            {
                Transform dot = dots[i];
                Vector3 position = dot.position - origin.position;
                Vector3 n = origin.up;
                float dist = position.x * n.x + position.y * n.y + position.z * n.z;
                Vector3 projectedPositions = origin.InverseTransformPointUnscaled(position - dist * n);
                Vector2 projectedPosition2 = new Vector2(projectedPositions.x, projectedPositions.z);
                float angle = Vector3.SignedAngle(pointer, projectedPosition2, Vector3.forward);
                if(angle > -90 && angle < -10)
                    _positionArray[i] = projectedPosition2 / relativeRadius;
            }

            for (int i = dots.Count; i < _positionArray.Length; i++)
            {
                _positionArray[i] = new Vector4(-10, -10);
            }
        
            sonar.SetVectorArray(Dots, _positionArray);
        }
    }
}