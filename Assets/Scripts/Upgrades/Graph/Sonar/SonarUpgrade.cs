﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Upgrades.Graph.Sonar {
    public class SonarUpgrade : UpgradeNode
    {
        public SonarEntityTracker sonarEntityTracker;
    
        protected override void OnUnlock()
        {
            sonarEntityTracker.enabled = true;
        }

        public override void OnEnableUpgrade()
        {
            OnUnlock();
        }

        public override void OnDisableUpgrade()
        {
            sonarEntityTracker.enabled = false;
        }
    }
}