﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    public class LayersManager : MonoBehaviour
    {
        public static LayersManager Instance { get; private set; }
        
        [SerializeField]
        private Transform player;
        
        [SerializeField]
        private ImageEffectApplier applier;
        
        public List<Layer> Layers => _layers;
        public Layer CurrentLayer => Layers[_current];
        
        private int _current = 0;

        public float lastY;
        public Material lastMaterial;

        public ParticleSystem particles;
        public Color particleDepthMultipler;
        private ParticleSystem.MinMaxGradient _particleColour;
        private Color _particleStartColour;
        private Color _particleEndColour;
        private ParticleSystem.MainModule _main;
        [SerializeField] private List<Layer> _layers;
        
        private void Awake()
        {
            
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogWarning($"Multiple objects of singleton {this} registered.");
            }
        }

        private void Start()
        {
            _main = particles.main;
            _particleColour = _main.startColor;
            _particleStartColour = _particleColour.colorMin;
            _particleEndColour = _particleColour.colorMax;
        }

        private void Update()
        {
            if (_current != 0 && player.position.y > Layers[_current].StartsAtY)
            {     
                _current--;
            }

            if (_current != Layers.Count - 1 && player.position.y < Layers[_current + 1].StartsAtY)
            {
                _current++;
            }

            applier.material.Lerp(Layers[_current].Material, _current == Layers.Count-1 ? lastMaterial : Layers[_current+1].Material, Mathf.InverseLerp(Layers[_current].StartsAtY, _current == Layers.Count-1 ? lastY : Layers[_current+1].StartsAtY, player.position.y));

            var pos = Mathf.Clamp01(Mathf.InverseLerp(Layers[0].StartsAtY, lastY, player.position.y));
            _particleColour.colorMin =
                _particleStartColour * (1 - pos) + particleDepthMultipler * pos;
            _particleColour.colorMax =
                _particleEndColour * (1 - pos) + particleDepthMultipler * pos;

            _main.startColor = _particleColour;
        }

        private void OnDrawGizmos()
        {
            Vector3 position = transform.position;
            position.y = 0;
            Gizmos.color = Color.white;
            for (int i = 0; i <= Layers.Count; i++)
            {
                Gizmos.DrawSphere(position + new Vector3(0, i == Layers.Count ? lastY : Layers[i].StartsAtY, 0), 3);
            }
            Gizmos.color = Color.green;
            for (int i = 0; i < Layers.Count; i++)
            {
                Gizmos.DrawLine(position + new Vector3(0, Layers[i].StartsAtY), position + new Vector3(0, i == Layers.Count-1 ? lastY : Layers[i+1].StartsAtY, 0));
            }
        }
    }

    [Serializable]
    public class Layer
    {
        public float StartsAtY;
        public Material Material;
    }
}