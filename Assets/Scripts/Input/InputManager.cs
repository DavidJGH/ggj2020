﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance { get; private set; }

    public event Action<Vector2> OnMouseMove;
    public event Action<float> OnXMove;
    public event Action<float> OnYMove;
    public event Action<float> OnZMove;
    public event Action OnLeftClickDown;
    public event Action OnRightClickDown;
    public event Action OnRightClickUp;

    private Vector2 _lastMousePos = new Vector2(.5f, .5f);
    private Vector2 _deltaMouse;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void FixedUpdate()
    {
        OnMouseMove?.Invoke(new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")));

        OnXMove?.Invoke(Input.GetAxisRaw("XAxis"));


        OnYMove?.Invoke(Input.GetAxisRaw("YAxis"));


        OnZMove?.Invoke(Input.GetAxisRaw("ZAxis"));
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnLeftClickDown?.Invoke();
        }

        if (Input.GetMouseButtonDown(1))
        {
            OnRightClickDown?.Invoke();
        }
        
        if (Input.GetMouseButtonUp(1))
        {
            OnRightClickUp?.Invoke();
        }
    }
}