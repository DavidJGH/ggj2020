namespace DefaultNamespace.Damage
{
    public interface IDamageable
    {
        float Health { get; set; }

        void OnDamaged(float hitValue);

        void OnRepaired(float repairValue);
    }
}