﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityComponent : MonoBehaviour
{

    
    private static float gravityAcceleration = -9.81f;
    private Rigidbody _rigidbody;
    
    [SerializeField]
    [Tooltip("density > 0: sinks; density = 0: no gravity; density < 0: rises to surface;")]
    private float _density;  

    // [SerializeField]
    // private float _drag;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        _rigidbody.velocity += Vector3.up * gravityAcceleration * _density * Time.fixedDeltaTime;
        //_rigidbody.velocity += transform.forward * -_drag * Time.fixedDeltaTime;  // Use Unity's drag for now.
    }
}
