using System;
using DefaultNamespace.Damage;
using Sirenix.Serialization;
using UnityEngine;
using Upgrades.Movement;

namespace Upgrades.Graph.Movement {
    [Serializable]
    public class MoveBackwardUpgrade : UpgradeNode, IMovementController, IScaleable {
        public float MaxVelocity {
            get => _maxVelocity * _scalingCoefficient;
            set => _maxVelocity = value;
        }

        public float DampingCoefficient {
            get => _dampingCoefficient * _scalingCoefficient;
            set => _dampingCoefficient = value;
        }

        [SerializeField]
        private float _maxVelocity = 3f;
        [SerializeField]
        private float _dampingCoefficient = 0.05f;
        [SerializeField]
        private float _scalingCoefficient = 1;

        private Rigidbody _rigidbody;

        protected override void OnUnlock() {
            _rigidbody = Transform.GetComponent<Rigidbody>();
        }

        public override void OnEnableUpgrade() {
            InputManager.Instance.OnZMove += MoveBackward;
        }

        private void MoveBackward(float value) {
            if (value >= 0) return;

            Vector3 currentVelocity = _rigidbody.velocity;
            Vector3 targetVelocity = Transform.forward * (MaxVelocity * value);
            _rigidbody.velocity = Vector3.Lerp(currentVelocity, targetVelocity, DampingCoefficient);

            OnMoveBackward?.Invoke(Transform.InverseTransformVector(_rigidbody.velocity).z);
        }

        public override void OnDisableUpgrade() {
            InputManager.Instance.OnZMove -= MoveBackward;
        }

        public event Action<float> OnMoveBackward;

        void IScaleable.ScaleValues(float coefficient) {
            _scalingCoefficient = coefficient;
        }
    }
}