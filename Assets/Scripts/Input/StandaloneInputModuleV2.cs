﻿using UnityEngine;
using UnityEngine.EventSystems;

public class StandaloneInputModuleV2 : StandaloneInputModule
{
    public GameObject GameObjectUnderPointer(int pointerId = kMouseLeftId)
    {
        var lastPointer = GetLastPointerEventData(pointerId);
        if (lastPointer != null)
            return lastPointer.pointerCurrentRaycast.gameObject;
        return null;

        return lastPointer.eligibleForClick ? lastPointer.pointerCurrentRaycast.gameObject : null;
    }
}