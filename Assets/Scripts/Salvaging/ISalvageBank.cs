using System;

namespace DefaultNamespace.Salvaging {
    public interface ISalvageBank {
        int Balance { get; set; }
        event Action<int> OnBalanceChange;
    }
}