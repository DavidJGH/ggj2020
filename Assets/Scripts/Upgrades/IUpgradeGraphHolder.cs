using System;
using System.Collections.Generic;
using Upgrades.Graph;

namespace Upgrades {
    public interface IUpgradeGraphHolder {
        UpgradeNode RootUpgrade { get; }
        ISet<UpgradeNode> GetPossibleUpgrades();
        void DisableUpgrades();
        void EnableUpgrades();
        T GetUpgradeOfType<T>() where T : UpgradeNode;
        UpgradeNode GetUpgradeOfType(Type type);
    }
}
