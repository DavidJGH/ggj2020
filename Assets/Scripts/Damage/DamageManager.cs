using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Random = System.Random;

namespace DefaultNamespace.Damage
{
    public class DamageManager : SerializedMonoBehaviour
    {
        public const int InitHealth = 1;
        public static DamageManager Instance { get; private set; }

        [OdinSerialize] public List<IDamageable> Damageables { get; private set; }

        private void Awake()
        {
            Instance = this;

            foreach (var damageable in Damageables)
            {
                damageable.Health = InitHealth;
            }
        }

        [Button(ButtonSizes.Large)]
        private void DebugRepair()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                if (Damageables.Count >= 1)
                {
                    Repair(Damageables[0], .5f);
                }
            }
        }

        public void Register(IDamageable damageable)
        {
            Damageables.Add(damageable);
            damageable.Health = InitHealth;
        }

        public void Damage(IDamageable damageable, float hit)
        {
            damageable.Health = damageable.Health - hit > 0 ? damageable.Health - hit : 0;
            damageable.OnDamaged(hit);
        }

        public void Repair(IDamageable damageable, float repairValue)
        {
            damageable.Health = damageable.Health + repairValue < 1 ? damageable.Health + repairValue : 1;
            damageable.OnRepaired(repairValue);
        }

        public void DamageRandom(float hitValue)
        {
            Damage(Damageables[new Random().Next(Damageables.Count)], hitValue);
        }
    }
}