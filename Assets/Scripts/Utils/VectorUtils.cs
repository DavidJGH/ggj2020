using UnityEngine;

namespace Utils
{
    public static class VectorUtils
    {
        public static Vector3 RandomIn(Bounds bounds) => new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z));

        public static float Volume(this Vector3 box) => box.x * box.y * box.z;
    }
}