﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Sirenix.OdinInspector;
using UnityEngine;

public class SwitchToCctvButton : MonoBehaviour, IClickable
{
    [SerializeField] [SceneObjectsOnly] private Camera outdoorCamera;
    [SerializeField] [SceneObjectsOnly] private Camera indoorCamera;
    [SerializeField] [SceneObjectsOnly] private Camera cctvCamera;

    public void OnClick()
    {
        cctvCamera.enabled = true;
        outdoorCamera.enabled = false;
        indoorCamera.enabled = false;
    }
}