using System;
using DefaultNamespace.Salvaging;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace DefaultNamespace.Damage
{
    public class CollisionDamager : SerializedMonoBehaviour, ICollisionDamager {
        [OdinSerialize] private float _maxHitValue = .5f;
        [OdinSerialize] private float _biggestImpact = 5f;

        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.GetComponent<Salvageable>() is null)
            {
                float impact =
                    Mathf.InverseLerp(0, _biggestImpact, Mathf.Clamp(other.impulse.magnitude, 0, _biggestImpact)) *
                    _maxHitValue;
                DamageManager.Instance.DamageRandom(impact);
                OnCollision?.Invoke(impact);
            }
        }

        public event Action<float> OnCollision;
    }
}