using System;
using System.Collections.Generic;
using DefaultNamespace.Salvaging;
using UnityEngine;
using Upgrades;
using Upgrades.Graph;

namespace Upgrades.Movement
{
    //TODO: do we need MovementUpgradeable?
    public class MovementUpgradeGraphHolder : UpgradeGraphHolder {
        public event Action<Vector3> OnMovement;

        private float _xVelocity;
        private float _yVelocity;
        private float _zVelocity;

        private float _maxVelocity;

        protected override void Awake()
        {
            base.Awake();

            var upgrades = new List<UpgradeNode>(RootUpgrade.GetAllChildren()) {RootUpgrade};

            //TODO: this is very ugly we should reconsider how we get the movement vector to the sound thingy
            /*foreach (var upgrade in upgrades)
            {
                switch (upgrade)
                {
                    case MoveForwardUpgrade forward:
                        if (_maxVelocity < forward.MaxVelocity) _maxVelocity = forward.MaxVelocity;
                        forward.OnMoveForward += velocity => _zVelocity = velocity;
                        break;
                    case MoveBackwardUpgrade backward:
                        if (_maxVelocity < backward.MaxVelocity) _maxVelocity = backward.MaxVelocity;
                        backward.OnMoveBackward += velocity => _zVelocity = velocity;
                        break;
                    case MoveLeftRightUpgrade leftRight:
                        if (_maxVelocity < leftRight.MaxVelocity) _maxVelocity = leftRight.MaxVelocity;
                        leftRight.OnMoveLeftOrRight += velocity => _xVelocity = velocity;
                        break;
                    case RiseSinkUpgrade riseSink:
                        if (_maxVelocity < riseSink.MaxVelocity) _maxVelocity = riseSink.MaxVelocity;
                        riseSink.OnRiseOrSink += velocity => _yVelocity = velocity;
                        break;
                }
            }*/
        }

        private void Start()
        {
            StationManager.Instance.stations[0].EnterStation += EnableUpgrades;
            StationManager.Instance.stations[0].ExitStation += DisableUpgrades;
        }

        private void OnDestroy()
        {
            var upgrades = new List<UpgradeNode>(RootUpgrade.GetAllChildren()) {RootUpgrade};

            /*foreach (var upgrade in upgrades)
            {
                switch (upgrade)
                {
                    case MoveForwardUpgrade forward:
                        forward.OnMoveForward -= SetZVelocity;
                        break;
                    case MoveBackwardUpgrade backward:
                        backward.OnMoveBackward -= SetZVelocity;
                        break;
                    case MoveLeftRightUpgrade leftRight:
                        leftRight.OnMoveLeftOrRight -= SetXVelocity;
                        break;
                    case RiseSinkUpgrade riseSink:
                        riseSink.OnRiseOrSink -= SetYVelocity;
                        break;
                }
            }*/
        }

        private void FixedUpdate()
        {
            OnMovement?.Invoke(new Vector3(_xVelocity, _yVelocity, _zVelocity) / _maxVelocity);
        }

        private void SetXVelocity(float velocity)
        {
            _xVelocity = velocity;
        }

        private void SetYVelocity(float velocity)
        {
            _yVelocity = velocity;
        }

        private void SetZVelocity(float velocity)
        {
            _zVelocity = velocity;
        }
    }
}